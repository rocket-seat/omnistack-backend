const Tweet = require('../models/Tweet')

module.exports = {
    async index(req, res) {
        const colTweets = await Tweet.find({}).sort('-createdAt');

        return res.json(colTweets);
    },

    async store(req, res) {
        const newTweet = await Tweet.create(req.body);

        // Avisa o front-end
        req.io.emit('novoTweet', newTweet);

        return res.json(newTweet);
    }
}