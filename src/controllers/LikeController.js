const Tweet = require('../models/Tweet')

module.exports = {
    async store(req, res) {
        const novoLike = await Tweet.findById(req.params.id);

        novoLike.set({
            likes: novoLike.likes + 1
        })

        await novoLike.save()

        // Avisa o front-end
        req.io.emit('novoLike', novoLike);

        return res.json(novoLike);
    }
}